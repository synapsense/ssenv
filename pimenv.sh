# NOTE: Source in your environment after ssenv.sh

PIM_HOME="$SRC/sz-80"
NMS_HOME="$PIM_HOME/nms"
CYNERGY_HOME="$PIM_HOME/cynergy"
PIM_DB_NAME=acmnms
PIM_DB_USERNAME=acmnms

alias cd-nms='pushd "$NMS_HOME"'
alias cd-nms-rest='pushd "$NMS_HOME/api-rest"'

function nms-update {
	_runin "$NMS_HOME" svn update
	_runin "$CYNERGY_HOME" svn update
}

function nms-merge-71 {
	local revision
	read -p "Enter nms revision: " revision
	_runin "$NMS_HOME" svn merge "^/branches/7.1.0/main@$revision"
	read -p "Enter cynergy revision: " revision
	_runin "$CYNERGY_HOME" svn merge "^/branches/7.1.0/main@$revision"
}

function nms-clean {
	_runin "$NMS_HOME" mvn clean
}

function nms-build {
	_runin "$NMS_HOME" mvn -D skipTests -Ddb_name=$PIM_DB_NAME -Ddb_username=$PIM_DB_USERNAME -P installer-dev,setup-tomcat,dev,webapp-mssql install || return
	_runin "$NMS_HOME/app-deployment" mvn cargo:deploy
}

function cynergy-clean {
	_runin "$CYNERGY_HOME/build" ant clean
}

function cynergy-build {
	_runin "$CYNERGY_HOME/build" ant build-and-war-and-copy
}

function nms-rebuild {
	nms-clean || return
	nms-build || return
	cynergy-clean || return
	cynergy-build
}

function nms-devtool {
	_runin "$NMS_HOME/devtools/buildscripts" cygstart cmd.exe /C devtool.bat
}

function nms-start-db {
	net start MSSQLSERVER
}

function nms-stop-db {
	net stop MSSQLSERVER
}

function nms-init-db {
	_runin "$NMS_HOME/db/xml" cygstart cmd.exe /C refreshmydb.bat
}

function nms-upgrade-db {
	_runin "$NMS_HOME/db/xml" cygstart cmd.exe /C upgrademydb.bat
}

# NOTE Opens in a window, must be stopped manually
function nms-start-lic {
	_runin "$NMS_HOME/tools/FlexNet_LicensingServer_11.4/bat" cygstart cmd.exe /C Start_FlexNet_Licensing_Server.bat
}

# NOTE Opens in a window, must be stopped manually
function nms-start-tc {
	rm -f "$(cygpath "$CATALINA_HOME")/logs"/*
	"$(cygpath "$CATALINA_HOME")/bin/startup.bat"
}

function nms-start {
	_service-running MSSQLSERVER || nms-start-db || return
	nms-start-lic || return
	nms-start-tc || return
	echo ""
	echo "All components started. Tomcat and FlexNet can be stopped with Ctrl-C in their terminal windows"
}
