#!/usr/bin/env bash

# Generates a minimal Java project with a Maven build

read -e -p "Group Id: " -i "com.panduit" GROUP_ID
read -e -p "Artifact Id: " -i "hello" ARTIFACT_ID
read -e -p "Version: " -i "1.0.0-SNAPSHOT" VERSION
read -e -p "Package: " -i "$GROUP_ID.$ARTIFACT_ID" PACKAGE
read -e -p "Class: " -i "Hello" CLASS
JUNIT_VERSION=4.12
JAVA_SOURCE=1.8

BASE_DIR=$ARTIFACT_ID
PACKAGE_DIR=${PACKAGE//\./\/}
TEST_CLASS=${CLASS}Test


if [[ -e $BASE_DIR ]]; then
	echo "$ARTIFACT_ID already exists"
	return 1
fi

mkdir -p "$BASE_DIR/src/main/java/$PACKAGE_DIR"
mkdir -p "$BASE_DIR/src/main/resources"
mkdir -p "$BASE_DIR/src/test/java/$PACKAGE_DIR"
mkdir -p "$BASE_DIR/src/test/resources"

cat > "$BASE_DIR/pom.xml" << EOF
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
		 xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd"
		 xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<modelVersion>4.0.0</modelVersion>

	<groupId>$GROUP_ID</groupId>
	<artifactId>$ARTIFACT_ID</artifactId>
	<version>$VERSION</version>

	<properties>
		<!-- dependency versions -->
		<junit.version>$JUNIT_VERSION</junit.version>
		<!-- plugin properties -->
		<project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
		<maven.compiler.source>$JAVA_SOURCE</maven.compiler.source>
		<maven.compiler.target>$JAVA_SOURCE</maven.compiler.target>
	</properties>

	<dependencies>
		<!-- test -->
		<dependency>
			<groupId>junit</groupId>
			<artifactId>junit</artifactId>
			<version>\${junit.version}</version>
			<scope>test</scope>
		</dependency>
	</dependencies>
</project>
EOF

cat > "$BASE_DIR/src/main/java/$PACKAGE_DIR/$CLASS.java" << EOF
package $PACKAGE;

public class $CLASS {
	public static String sayHello() {
		return "Hello, world!";
	}
}
EOF

cat > "$BASE_DIR/src/test/java/$PACKAGE_DIR/$TEST_CLASS.java" << EOF
package $PACKAGE;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class $TEST_CLASS {
	@Test
	public void sayHello() throws Exception {
		assertEquals("Hello, world!", $CLASS.sayHello());
	}
}
EOF

