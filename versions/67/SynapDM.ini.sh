# This script is sourced from ssenv.sh
# I haven't used a sed script since it cannot take arguments (some files need that)

# NOTE: This script assumes a DOS format and preserves that format
# NOTE: This script is idempotent (can be safely run multiple times)

_OSS_HOME_WIN=$(_escape-backslash $(cygpath -aw "$OSS_HOME"))
_ISS_HOME_WIN=$(_escape-backslash $(cygpath -aw "$ISS_HOME"))
sed -b -i -r \
    -e "s/$_OSS_HOME_WIN/$_ISS_HOME_WIN/g" \
    -e "s/^(Maximum Version)=.*/\1=1.7\r/" \
    -e "s/^(Virtual Machine Parameters)=(-agentlib:jdwp\S+ )?(.+)/\1=-agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=5000 \3/" \
    "$ISS_DM/SynapDM.ini"
