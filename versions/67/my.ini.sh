# This script is sourced from ssenv.sh
# I haven't used a sed script since it cannot take arguments (some files need that)

# NOTE: This script assumes a DOS format and preserves that format

sed -b -i -r \
    -e "s/^(basedir)=.*/#\1=\"unset\"\r/" \
    -e "s/^(datadir)=.*/\1=\"data\"\r/" \
    "$ISS_DB/my.ini"
