# This script is sourced from ssenv.sh
# I haven't used a sed script since it cannot take arguments (some files need that)

# NOTE: This script assumes a DOS format and preserves that format
# NOTE: This script is idempotent (can be safely run multiple times)

# Set JAVA_HOME
sed -b -i -r \
    -e "/JAVA_HOME=/c set \"JAVA_HOME=$(_escape-backslash "$JAVA_HOME")\"" \
    "$ISS_ES/bin/standalone.conf.bat"
