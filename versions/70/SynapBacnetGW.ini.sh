# This script is sourced from ssenv.sh
# I haven't used a sed script since it cannot take arguments (some files need that)

# NOTE: This script assumes a DOS format and preserves that format
# NOTE: This script is idempotent (can be safely run multiple times)

# Return if BG was not installed
[[ -f "$ISS_BG/SynapBacnetGW.ini" ]] || return 0

_OSS_HOME_WIN=$(_escape-backslash $(cygpath -aw "$OSS_HOME"))
_ISS_HOME_WIN=$(_escape-backslash $(cygpath -aw "$ISS_HOME"))
sed -b -i -r \
    -e "s/$_OSS_HOME_WIN/$_ISS_HOME_WIN/g" \
    -e "/Product Information/i Virtual Machine Parameters=-agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=5003 -Dcom.sun.management.jmxremote.port=2103 -Dcom.sun.management.jmxremote.authenticate=false -Dcom.sun.management.jmxremote.ssl=false \r" \
    "$ISS_BG/SynapBacnetGW.ini"
