# This script is sourced from ssenv.sh
# I haven't used a sed script since it cannot take arguments (some files need that)

# NOTE: This script assumes a DOS format and preserves that format
# NOTE: This script is idempotent (can be safely run multiple times)

# Return if MS was not installed
[[ -f "$(iss-ms)/MapSense.ini" ]] || return 0

_OSS_HOME_WIN=$(_escape-backslash $(cygpath -aw "$OSS_HOME"))
_ISS_HOME_WIN=$(_escape-backslash $(cygpath -aw "$ISS_HOME"))
sed -b -i -r \
    -e "s/$_OSS_HOME_WIN/$_ISS_HOME_WIN/g" \
    "$(iss-ms)/MapSense.ini"
