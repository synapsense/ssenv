SRC="/cygdrive/c/src"
WORK="/cygdrive/c/work"
OLD_GIT_URL="ssh://git.synapsense.int/git"
NEW_GIT_URL="git@bitbucket.org:synapsense"
TOOLS="git.synapsense.int:/git/3rdpartytools"
SS_ENV="$SRC/ssenv"

# Validate SS_ENV
if [[ ! -f "$SS_ENV/ssenv.sh" ]]; then
	echo "ssenv.sh cannot be found in $SS_ENV. The ss-xxx commands will not be available, please review your environment."
	return 1
fi

_REG_SYSTEM_VARIABLES='HKLM\SYSTEM\CurrentControlSet\Control\Session Manager\Environment'
_OFFICIAL_SERVICES=('SynapSense DB' 'SYNAPSERVER' 'synapdm' 'synapli' 'SynapBACnet' 'synapmbgw' 'synapsnmp')
_OFFICIAL_SERVICES_REVERSED=('synapsnmp' 'synapmbgw' 'SynapBACnet' 'synapli' 'synapdm' 'SYNAPSERVER' 'SynapSense DB')
_ISS_SERVICES_PREFIX=('ssdb' 'sses' 'ssdm' 'ssli' 'ssbg' 'ssmg' 'sssg')
_ISS_SERVICES_PREFIX_REVERSED=('sssg' 'ssmg' 'ssbg' 'ssli' 'ssdm' 'sses' 'ssdb')
# Modbus and SNMP gateways are not supported in dev because they don't have a Maven build.
_SS_SERVICES_PREFIX=('ssdb' 'sses' 'ssdm' 'ssli' 'ssbg')
_SS_SERVICES_PREFIX_REVERSED=('ssbg' 'ssli' 'ssdm' 'sses' 'ssdb')
_SS_GIT_REPOS=()

alias cd-ssenv="cd $SS_ENV"
alias cd-work="cd $WORK"

alias cd-70='cd $WORK/ss-70'
function iss-switch-70 {
	_activate_java8
	ISS_PROFILE=70
	_iss-init
}

alias cd-68='cd $WORK/ss-68'
function iss-switch-68 {
	_activate_java7
	ISS_PROFILE=68
	_iss-init
}

alias cd-67='cd $WORK/ss-67'
function iss-switch-67 {
	_activate_java7
	ISS_PROFILE=67
	_iss-init
}

function ss-switch-master {
	_activate_java8
	SS_PROFILE=master
	_SS_MYSQL_DIST="mariadb-5.5.32-winx64.zip"
	_SS_JBOSS_DIST="jboss-as-7.1.1.Final.zip"
	_ss-init
}

function ss-switch-selene {
	_activate_java7
	SS_PROFILE=selene
	_SS_MYSQL_DIST="mariadb-5.5.32-winx64.zip"
	_SS_JBOSS_DIST="jboss-as-7.1.1.Final.zip"
	_ss-init
}

alias grep-errors="grep -E -B 1 '(Failures|Errors): [^0]'"
alias sed-errors="sed -n -r \
	-e 's/, Time elapsed: .+ sec//' \
	-e '/^(Running|Tests run|There are test failures|Please refer to)/p' \
	-e '/Building .*-SNAPSHOT$/p'"


############################################################
# oss-xxx functions operating on the official installation
############################################################

OSS_HOME='/cygdrive/c/Program Files/SynapSense'
OSS_DB="$OSS_HOME/SynapSense DB"
OSS_ES="$OSS_HOME/jboss-as-7.1.1.Final"
OSS_DM="$OSS_HOME/SynapDM"
OSS_LI="$OSS_HOME/SynapViz"
OSS_BG="$OSS_HOME/BACnet GW"
OSS_MG="$OSS_HOME/SynapModbus"
OSS_SG="$OSS_HOME/SynapSNMP"

# MapSense home is evaluated on every use. Expansion would not work when then directory is initially missing.
function oss-ms {
	echo "$OSS_HOME"/MapSense*
}


# Removes all logs for $ISS_PROFILE
function oss-rm-logs {
	rm -f "$OSS_DB/data"/*.err
	rm -f "$OSS_ES/standalone/log"/*
	rm -f "$OSS_DM/logs"/*
	rm -f "$OSS_DM/data"/*
	rm -f "$OSS_LI/logs"/*
	rm -f "$OSS_BG/logs"/*
	rm -f "$OSS_MG/logs"/*
	rm -f "$OSS_SG/logs"/*
}

# Starts all services of the official installation
function oss-start {
	oss-rm-logs
	_start-services "${_OFFICIAL_SERVICES[@]}"
}

# Starts all services for $ISS_PROFILE with a pause after ES
function oss-start-delay {
	oss-rm-logs
	_start-services "SynapSense DB" "SYNAPSERVER"
	sleep 20
	_start-services "synapdm" "synapli" "SynapBACnet" "synapmbgw" "synapsnmp"
}

# Stops all services of the official installation
function oss-stop {
	_stop-services "${_OFFICIAL_SERVICES_REVERSED[@]}"
}

# Restarts all services of the official installation
function oss-restart {
	oss-stop
	oss-start
}

# Starts MapSense for the official installation
function oss-mapsense {
	cygstart -d "$(oss-ms)" "$(oss-ms)/MapSense.exe" -occulo
}

############################################################
# iss-xxx functions operating on an installation profile
############################################################

# Display the current installation profile
function iss-status {
	if [[ -n $ISS_PROFILE ]]; then
		echo "The active installation profile is '$ISS_PROFILE'"
	else
		echo "There's no active installation profile"
	fi
}

# Initializes private variables based on ISS_PROFILE
function _iss-init {
	if [[ -z $ISS_PROFILE ]]; then
		echo "There's no active installation profile. Run iss-switch-<profile>."
		return 1
	fi
	ISS_HOME="$WORK/ss-$ISS_PROFILE"
	ISS_DB="$ISS_HOME/SynapSense DB"
	ISS_ES="$ISS_HOME/jboss-as-7.1.1.Final"
	ISS_DM="$ISS_HOME/SynapDM"
	ISS_LI="$ISS_HOME/SynapViz"
	ISS_BG="$ISS_HOME/BACnet GW"
	ISS_MG="$ISS_HOME/SynapModbus"
	ISS_SG="$ISS_HOME/SynapSNMP"
	# Service names
	_ISS_SERVICES=()
	local svc
	for svc in "${_ISS_SERVICES_PREFIX[@]}"; do
		_ISS_SERVICES+=("$svc-$ISS_PROFILE")
	done
	_ISS_SERVICES_REVERSED=()
	for svc in "${_ISS_SERVICES_PREFIX_REVERSED[@]}"; do
		_ISS_SERVICES_REVERSED+=("$svc-$ISS_PROFILE")
	done
}

# MapSense home is evaluated on every use. Expansion would not work when then directory is initially missing.
function iss-ms {
	echo "$ISS_HOME"/MapSense*
}

# Changes an official installation into a profile installation
function iss-install {
	_iss-init || return
	# Remove previous installation if necessary
	if [[ -d "$ISS_HOME" ]]; then
		iss-remove || return
	fi
	# Stop official services
	oss-stop
	# Copy over the official installation
	cp -prT "$OSS_HOME" "$ISS_HOME"
	# Copy the init_db.bat scripts so we can re-create the DB (not necessary since 7.0.0)
	if [[ -f "$SS_ENV/versions/$ISS_PROFILE/init_db.bat" ]]; then
		cp "$SS_ENV/versions/$ISS_PROFILE"/init_db*.bat "$ISS_DB/scripts" || return
	fi
	# Customize cfg files
	source "$SS_ENV/versions/$ISS_PROFILE/my.ini.sh" || return
	source "$SS_ENV/versions/$ISS_PROFILE/standalone.conf.bat.sh" || return
	source "$SS_ENV/versions/$ISS_PROFILE/SynapDM.ini.sh" || return
	source "$SS_ENV/versions/$ISS_PROFILE/SynapLI.ini.sh" || return
	source "$SS_ENV/versions/$ISS_PROFILE/MapSense.ini.sh" || return
	source "$SS_ENV/versions/$ISS_PROFILE/SynapBacnetGW.ini.sh" || return
	source "$SS_ENV/versions/$ISS_PROFILE/SynapModbusGW.ini.sh" || return
	source "$SS_ENV/versions/$ISS_PROFILE/SynapSNMP.ini.sh" || return
	# Clean up environment variables (not necessary since 6.8)
	if [[ $ISS_PROFILE == 67 ]]; then
		reg delete "$_REG_SYSTEM_VARIABLES" /v JBOSS_HOME  /f
	fi
	# Install the services
	"$ISS_DB/bin/mysqld" --install-manual ssdb-$ISS_PROFILE --defaults-file="$(cygpath -aw "$ISS_DB/my.ini")" || return
	"$ISS_ES/bin/jbosssvc" -im sses-$ISS_PROFILE service.bat || return
	# For the rest, make sure the component is installed before creating a Windows service.
	[[ ! -f "$ISS_DM/SynapDM.exe" ]] || sc create "ssdm-$ISS_PROFILE" binPath= "$(cygpath -aw "$ISS_DM/SynapDM.exe")" || return
	[[ ! -f "$ISS_LI/SynapLI.exe" ]] || sc create "ssli-$ISS_PROFILE" binPath= "$(cygpath -aw "$ISS_LI/SynapLI.exe")" || return
	[[ ! -f "$ISS_BG/SynapBacnetGW.exe" ]] || sc create "ssbg-$ISS_PROFILE" binPath= "$(cygpath -aw "$ISS_BG/SynapBacnetGW.exe")" || return
	[[ ! -f "$ISS_MG/SynapModbusGW.exe" ]] || sc create "ssmg-$ISS_PROFILE" binPath= "$(cygpath -aw "$ISS_MG/SynapModbusGW.exe")" || return
	[[ ! -f "$ISS_SG/SynapSNMP.exe" ]] || sc create "sssg-$ISS_PROFILE" binPath= "$(cygpath -aw "$ISS_SG/SynapSNMP.exe")" || return
	if [[ $ISS_PROFILE == 67 ]]; then
		echo "The official installation was copied to $ISS_HOME. Now:"
		echo " - Uninstall the official instalation. This will remove JAVA_HOME."
		echo " - Restore JAVA_HOME. Ex: reg add 'HKLM\SYSTEM\CurrentControlSet\Control\Session Manager\Environment' /v JAVA_HOME /t REG_SZ /d 'C:\tools\java'"
		echo " - Remove the leftovers of the official installation: rm -rf \"\$OSS_HOME\""
		echo " - Start the services with iss-start-delay and stop them with iss-stop."
		echo " - Start MapSense with iss-mapsense."
	else
		echo "The official installation was copied to $ISS_HOME. Now:"
		echo " - Uninstall the official instalation."
		echo " - Remove the leftovers of the official installation: rm -rf \"\$OSS_HOME\""
		echo " - Start the services with iss-start-delay and stop them with iss-stop."
		echo " - Start MapSense with iss-mapsense."
	fi

}

# Removes all logs for $ISS_PROFILE
function iss-rm-logs {
	_iss-init || return
	rm -f "$ISS_DB/data"/*.err
	rm -f "$ISS_ES/standalone/log"/*
	rm -f "$ISS_DM/logs"/*
	rm -f "$ISS_DM/data"/*
	rm -f "$ISS_LI/logs"/*
	rm -f "$ISS_BG/logs"/*
	rm -f "$ISS_MG/logs"/*
	rm -f "$ISS_SG/logs"/*
}

# Starts all services for $ISS_PROFILE
function iss-start {
	_iss-init || return
	iss-rm-logs
	_start-services "${_ISS_SERVICES[@]}"
}

# Starts all services for $ISS_PROFILE with a pause after ES
function iss-start-delay {
	_iss-init || return
	iss-rm-logs
	_start-services "ssdb-$ISS_PROFILE" "sses-$ISS_PROFILE"
	sleep 20
	_start-services "ssdm-$ISS_PROFILE" "ssli-$ISS_PROFILE" "ssbg-$ISS_PROFILE" "ssmg-$ISS_PROFILE" "sssg-$ISS_PROFILE"
}

# Stops all services for $ISS_PROFILE
function iss-stop {
	_iss-init || return
	_stop-services "${_ISS_SERVICES_REVERSED[@]}"
}

# Restart all services for $ISS_PROFILE
function iss-restart {
	iss-stop
	iss-start
}

# Invokes init_db.bat for $ISS_PROFILE
function iss-init-db {
	_iss-init || return
	net start "ssdb-$ISS_PROFILE"
	_SAVED_PATH=$PATH
	export PATH="$ISS_DB/bin/:$PATH"
	"$ISS_DB/scripts/init_db_dev.bat"
	local STATUS=$?
	export PATH="$_SAVED_PATH"
	net stop "ssdb-$ISS_PROFILE"
	return $STATUS
}

# Removes all SS services for profile $ISS_PROFILE
function iss-remove-services {
	_iss-init || return
	_remove-services "${_ISS_SERVICES_REVERSED[@]}"
}

# Performs a complete uninstall for installation profile $ISS_PROFILE.
# It stops and removes all related services and then deletes the installation directory.
function iss-remove {
	_iss-init || return
	iss-remove-services
	_rm-safely "$ISS_HOME"
}

# Starts MapSense for profile $ISS_PROFILE.
function iss-mapsense {
	_iss-init || return
	cygstart -d "$(iss-ms)" "$(iss-ms)/MapSense.exe" -occulo
}


############################################################
# ss-xxx functions operating on a development profile
############################################################

# Display the current development profile
function ss-status {
	if [[ -n $SS_PROFILE ]]; then
		echo "The active development profile is '$SS_PROFILE'"
	else
		echo "There's no active development profile"
	fi
}

# Initializes private variables based on SS_PROFILE
function _ss-init {
	if [[ -z $SS_PROFILE ]]; then
		echo "There's no active development profile. Run ss-switch-<profile>."
		return 1
	fi
	SS_SRC_MIDDLE="$SRC/ss-$SS_PROFILE/middle"
	SS_SRC_MAPSENSE="$SRC/ss-$SS_PROFILE/mapsense"
	SS_SRC_WEBAPPS="$SRC/ss-$SS_PROFILE/webapps"
	SS_SRC_SHADOWWCAT="$SRC/ss-$SS_PROFILE/shadowwcat"
	_SS_GIT_REPOS=("$SS_SRC_MIDDLE" "$SS_SRC_MAPSENSE" "$SS_SRC_WEBAPPS")

	SS_HOME="$SS_SRC_MIDDLE/runtime"
	SS_DB="$SS_HOME/SynapSense DB"
	SS_ES="$SS_HOME/jboss-as-7.1.1.Final"
	SS_DM="$SS_HOME/SynapDM"
	SS_LI="$SS_HOME/SynapViz"
	SS_BG="$SS_HOME/BACnet GW"
	# Service names
	_SS_SERVICES=()
	local svc
	for svc in "${_SS_SERVICES_PREFIX[@]}"; do
		_SS_SERVICES+=("$svc-$SS_PROFILE")
	done
	_SS_SERVICES_REVERSED=()
	for svc in "${_SS_SERVICES_PREFIX_REVERSED[@]}"; do
		_SS_SERVICES_REVERSED+=("$svc-$SS_PROFILE")
	done
	mkdir -pv "$SS_ENV/tools"
	# SS_PROFILE must be exported so it can activate the maven "development" profile
	export SS_PROFILE
}

# clones middle, webapps and mapsense repositories into $SRC/ss-$SS_PROFILE/<project>
function ss-clone {
	_ss-init || return;

	_SS_REPODIRS+=($SS_SRC_MIDDLE $SS_SRC_MAPSENSE $SS_SRC_WEBAPPS $SS_SRC_SHADOWWCAT)
	if [[ $SS_PROFILE == selene ]]; then
		_SS_REPOS+=("$OLD_GIT_URL/middle.git")
		_SS_REPOS+=("$OLD_GIT_URL/tools/deploymentlab.git")
		_SS_REPOS+=("$OLD_GIT_URL/webapps.git")
		_SS_REPOS+=("$OLD_GIT_URL/shadowwcat.git")
	else
		_SS_REPOS+=("$NEW_GIT_URL/middle.git")
		_SS_REPOS+=("$NEW_GIT_URL/middle.git")
		_SS_REPOS+=("$NEW_GIT_URL/webapps.git")
		_SS_REPOS+=("$NEW_GIT_URL/shadowwcat.git")
	fi

	for index in 0 1 2 3; do
		[[ -d ${_SS_REPODIRS[index]} ]] || git clone ${_SS_REPOS[index]} "${_SS_REPODIRS[index]}" || return
		_runin "${_SS_REPODIRS[index]}" git checkout $SS_PROFILE || return
	done
}

function cd-middle {
	_ss-init || return
	pushd $SS_SRC_MIDDLE
}

function cd-webconsole {
	_ss-init || return
	pushd $SS_SRC_WEBAPPS/webconsole
}

function cd-mapsense {
	_ss-init || return
	pushd $SS_SRC_MAPSENSE
}

function cd-shadowwcat {
	_ss-init || return
	pushd $SS_SRC_SHADOWWCAT
}

function cd-runtime {
	_ss-init || return
	pushd $SS_HOME
}

function ss-rm-logs {
	_ss-init || return
	rm -f "$SS_DB/data"/*.err
	rm -f "$SS_ES/standalone/log"/*
	rm -f "$SS_DM/logs"/*
	rm -f "$SS_DM/data"/*
	rm -f "$SS_LI/logs"/*
	rm -f "$SS_BG/logs"/*
}

# Starts all services for $SS_PROFILE
function ss-start {
	_ss-init || return
	ss-rm-logs
	_start-services "${_SS_SERVICES[@]}"
}

function ss-start-delay {
	_ss-init || return
	ss-rm-logs
	_start-services "ssdb-$SS_PROFILE" "sses-$SS_PROFILE" || return
	sleep 20
	_start-services "ssdm-$SS_PROFILE" "ssli-$SS_PROFILE" "ssbg-$SS_PROFILE"
}

# Stops all services for $SS_PROFILE
function ss-stop {
	_ss-init || return
	_stop-services "${_SS_SERVICES_REVERSED[@]}"
}

# Restart all services for $SS_PROFILE
function ss-restart {
	ss-stop
	ss-start
}

# Installs a DB (and the corresponding service) in the runtime for $SS_PROFILE
function ss-install-db {
	_ss-init || return
	# Extract the DB distribution
	[[ -f "$SS_ENV/tools/$_SS_MYSQL_DIST" ]] || scp "$TOOLS/$_SS_MYSQL_DIST" "$SS_ENV/tools" || return
	unzip -q "$SS_ENV/tools/$_SS_MYSQL_DIST" -d "$SS_HOME" || return
	mv "$SS_HOME/${_SS_MYSQL_DIST%.*}" "$SS_DB" || return
	# Copy my.ini (we don't need customization since 7.0.0)
	if [[ $SS_PROFILE == selene ]]; then
		cp "$SS_ENV/branches/$SS_PROFILE/my.ini" "$SS_DB" || return
	else
		cp "$SS_SRC_MIDDLE/Database/MySQL/my.ini" "$SS_DB" || return
	fi
	# Copy the DB scripts
	cp -prT "$SS_SRC_MIDDLE/Database/MySQL" "$SS_DB/scripts" || return
	# Install the service
	"$SS_DB/bin/mysqld" --install-manual ssdb-$SS_PROFILE --defaults-file="$(cygpath -aw "$SS_DB/my.ini")" || return
	sc qc "ssdb-$SS_PROFILE" || return
	# Set root password
	net start "ssdb-$SS_PROFILE" || return
	# Wait for mariadb to initialize the log file
    until "$SS_DB/bin/mysqladmin" --user root status >/dev/null 2>&1; do
        echo "MySql not available. Retrying after 1 sec ...";
        sleep 1;
    done
	"$SS_DB/bin/mysql" -u root -e "set password = password('dbuser')" || return
	# Create SynapSense DB
	ss-init-db
}

# Invokes init_db.bat in the runtime for $SS_PROFILE
# It assumes the maven build has already copied the scripts to runtime
function ss-init-db {
	_ss-init || return
	_service-running "ssdb-$SS_PROFILE" || net start "ssdb-$SS_PROFILE"
	_SAVED_PATH=$PATH
	export PATH="$SS_DB/bin/:$PATH"
	"$SS_DB/scripts/init_db_dev.bat"
	local STATUS=$?
	export PATH="$_SAVED_PATH"
	net stop "ssdb-$SS_PROFILE"
	return $STATUS
}

# Removes the DB (and the corresponding service) from the runtime for $SS_PROFILE
function ss-remove-db {
	_ss-init || return
	_remove-services "ssdb-$SS_PROFILE"
	# First remove $SS_DB/include without confirmation to avoid prompting for write-protected files.
	rm -rf "$SS_DB/include"
	_rm-safely "$SS_DB"
}

# Installs ES (and the corresponding service) in the runtime for $SS_PROFILE
function ss-install-es {
	_ss-init || return
	# Extract the JBoss distribution
	[[ -f "$SS_ENV/tools/$_SS_JBOSS_DIST" ]] || scp "$TOOLS/$_SS_JBOSS_DIST" "$SS_ENV/tools" || return
	unzip -q "$SS_ENV/tools/$_SS_JBOSS_DIST" -d "$SS_HOME" || return
	# Note: As of 7.0, there's no need to rename.
	mv "$SS_HOME/${_SS_JBOSS_DIST%.*}" "$SS_ES" || return
	# Customize installation
	ss-customize-jboss || return
	# Package ES
	_runin "$SS_SRC_MIDDLE/DeviceManager" mvn -D skipTests -pl :dm,:dm-plugin-api install || return
	_runin "$SS_SRC_MIDDLE/es" mvn -D skipTests install || return
	if [[ $SS_PROFILE == selene ]]; then
		"$SS_ES/bin/jbosssvc" -im "sses-$SS_PROFILE" service.bat
	else
		"$SS_ES/bin/service.bat" install
	fi
}

# Copy JBoss customization files from source to runtime for $SS_PROFILE
# JBoss must not be running when the files are copied.
function ss-customize-jboss {
	_ss-init || return
	# Overlay customized files
	cp -prT "$SS_SRC_MIDDLE/es/es-ear/JBoss" "$SS_ES" || return
	# Set JAVA_HOME in standalone.conf.bat
	sed -b -i -r \
		-e "/JAVA_HOME=/c set \"JAVA_HOME=$(_escape-backslash "$JAVA_HOME")\"" \
		"$SS_ES/bin/standalone.conf.bat" || return
	# Set service name in service.bat (no-op on selene)
	sed -b -i -r \
		-e "s/@SVCNAME@/sses-$SS_PROFILE/" \
		"$SS_ES/bin/service.bat" || return
}

# Removes ES (and the corresponding service) from the runtime for $SS_PROFILE
function ss-remove-es {
	_ss-init || return
	_remove-services "sses-$SS_PROFILE"
	_rm-safely "$SS_ES"
}

# Installs DM (and the corresponding service) in the runtime for profile $SS_PROFILE
function ss-install-dm {
	_ss-init || return
	mkdir -pv "$SS_DM/conf" || return
	# Copy AdvancedInstaller files
	cp -puv "$SS_ENV/branches/$SS_PROFILE/SynapDM.exe" "$SS_DM" || return
	sed -b "s|@SS_DM@|$(_escape-backslash $(cygpath -aw "$SS_DM"))|g" "$SS_ENV/branches/$SS_PROFILE/SynapDM.ini" > "$SS_DM/SynapDM.ini" || return
	# Copy the default configuration including the SimulatorPlugin and all transports
	cp -npv "$SS_ENV/branches/$SS_PROFILE/dm.conf" "$SS_DM/conf" || return
	# Package DM
	_runin "$SS_SRC_MIDDLE" mvn -N install || return
	_runin "$SS_SRC_MIDDLE/DeviceManager" mvn -D skipTests -pl :dm,:dm-plugin-api install || return
	_runin "$SS_SRC_MIDDLE/es" mvn -D skipTests -pl :es,:es-api install || return
	_runin "$SS_SRC_MIDDLE/sz8" mvn -D skipTests install || return
	_runin "$SS_SRC_MIDDLE/BACnetGateway" mvn -D skipTests install || return
	_runin "$SS_SRC_MIDDLE/DeviceManager" mvn -D skipTests install || return
	# Install the service
	sc create "ssdm-$SS_PROFILE" binPath= "$(cygpath -aw "$SS_DM/SynapDM.exe")"
}

# Removes DM (and the corresponding service) from the runtime for profile $SS_PROFILE
function ss-remove-dm {
	_ss-init || return
	_remove-services "ssdm-$SS_PROFILE"
	_rm-safely "$SS_DM"
}

# Installs LI (and the corresponding service) in the runtime for profile $SS_PROFILE
function ss-install-li {
	_ss-init || return
	mkdir -pv "$SS_LI" || return
	# Copy AdvancedInstaller files
	cp -puv "$SS_ENV/branches/$SS_PROFILE/SynapLI.exe" "$SS_LI" || return
	sed -b "s|@SS_LI@|$(_escape-backslash $(cygpath -aw "$SS_LI"))|g" "$SS_ENV/branches/$SS_PROFILE/SynapLI.ini" > "$SS_LI/SynapLI.ini" || return
	# Package LI
	_runin "$SS_SRC_MIDDLE/DeviceManager" mvn -D skipTests -pl :dm,:dm-plugin-api install || return
	_runin "$SS_SRC_MIDDLE/es" mvn -D skipTests -pl :es,:es-api install || return
	_runin "$SS_SRC_MIDDLE/LiveImaging" mvn -D skipTests install || return
	# Install the service
	sc create "ssli-$SS_PROFILE" binPath= "$(cygpath -aw "$SS_LI/SynapLI.exe")"
}

# Removes LI (and the corresponding service) from the runtime for profile $SS_PROFILE
function ss-remove-li {
	_ss-init || return
	_remove-services "ssli-$SS_PROFILE"
	_rm-safely "$SS_LI"
}

# Installs BG (and the corresponding service) in the runtime for profile $SS_PROFILE
function ss-install-bg {
	_ss-init || return
	mkdir -pv "$SS_BG" || return
	# Copy AdvancedInstaller files
	cp -puv "$SS_ENV/branches/$SS_PROFILE/SynapBacnetGW.exe" "$SS_BG" || return
	sed -b "s|@SS_BG@|$(_escape-backslash $(cygpath -aw "$SS_BG"))|g" "$SS_ENV/branches/$SS_PROFILE/SynapBacnetGW.ini" > "$SS_BG/SynapBacnetGW.ini" || return
	# Package BG
	_runin "$SS_SRC_MIDDLE/DeviceManager" mvn -D skipTests -pl :dm,:dm-plugin-api install || return
	_runin "$SS_SRC_MIDDLE/es" mvn -D skipTests -pl :es,:es-api install || return
	_runin "$SS_SRC_MIDDLE/BACnetGateway" mvn -D skipTests install || return
	# Install the service
	sc create "ssbg-$SS_PROFILE" binPath= "$(cygpath -aw "$SS_BG/SynapBacnetGW.exe")"
}

# Removes BG (and the corresponding service) from the runtime for profile $SS_PROFILE
function ss-remove-bg {
	_ss-init || return
	_remove-services "ssbg-$SS_PROFILE"
	_rm-safely "$SS_BG"
}

# Removes all 4 SS services from the runtime for profile $SS_PROFILE
function ss-remove-services {
	_ss-init || return
	_remove-services "${_SS_SERVICES_REVERSED[@]}"
}

# Builds and performs a complete install for profile $SS_PROFILE.
# If you run this multiple times, use ss-remove to clean the environment.
function ss-install {
	_ss-init || return
	ss-install-db || return
	ss-install-es || return
	ss-install-dm || return
	ss-install-li || return
	ss-install-bg || return
	echo "Runtime image created. Services can be started with ss-start."
}

# Performs a complete uninstall for profile $SS_PROFILE.
# It stops and removes all related services and then deletes the runtime directory.
function ss-remove {
	_ss-init || return
	ss-remove-bg
	ss-remove-li
	ss-remove-dm
	ss-remove-es
	ss-remove-db
	_rm-safely "$SS_HOME"
}

# Builds webconsole and deploys it to JBoss for profile $SS_PROFILE.
function ss-webconsole {
	_ss-init || return
	_runin "$SS_SRC_WEBAPPS/webconsole" ant "-DJBossHome=$(cygpath -aw "$SS_ES")" deploy
}

# Starts MapSense for profile $SS_PROFILE.
function ss-mapsense {
	_ss-init || return
	cygstart -d "$SS_SRC_MAPSENSE" --minimize ant.bat run_developer
}

# Clears MapSense registry cfg of the SmartZone configuration
function ss-clear-mapsense {
	reg delete 'HKLM\SOFTWARE\JavaSoft\Prefs' /v '/Smart/Zone/Host' /f
	reg delete 'HKLM\SOFTWARE\JavaSoft\Prefs' /v '/Smart/Zone/Port' /f
}

# Downloads MapSense distribution from Jenkins
function ss-get-mapsense {
	_ss-init || return
	mkdir -p "$SS_SRC_MAPSENSE/dist"
	# We specify then full path, otherwise wget does not overwrite then existing file
	wget -O "$SS_SRC_MAPSENSE/dist/deploymentlab.zip" \
		"http://svrbld4/job/.MapSense/lastSuccessfulBuild/artifact/dist/deploymentlab.zip"
}

# Clones all repositories and builds the projects for profile $SS_PROFILE.
function ss-quickstart {
	_ss-init || return
	ss-clone || return
	ss-install || return
	ss-webconsole || return
	ss-start || return
	cygstart http://localhost:8080/synapsoft
}

# Performs case clean build on all projects.
function ss-build {
	_ss-init || return
	_runin "$SS_SRC_MIDDLE" mvn -DskipTests clean install || return
	ss-webconsole || return
	if [[ $SS_PROFILE == selene ]]; then
		_runin "$SS_SRC_MAPSENSE" ant clean get_arts compile_ss
	else
		_runin "$SS_SRC_MAPSENSE" ant clean get_arts build
	fi
}

# Performs a git status on all repositories.
function ss-git-status {
	_ss-init || return
	for dir in "${_SS_GIT_REPOS[@]}"; do
		_runin "$dir" git status || return
	done
}

# Performs a git pull on all repositories. Local commits are rebased.
function ss-git-pull {
	_ss-init || return
	for dir in "${_SS_GIT_REPOS[@]}"; do
		_runin "$dir" git fetch || return
		_runin "$dir" git rebase --autostash || return
	done
}

# Lists all local commits to be pushed to remote for all repositories.
function ss-git-push-log {
	_ss-init || return
	for dir in "${_SS_GIT_REPOS[@]}"; do
		_runin "$dir" git log @{u}.. || return
	done
}

# Performs a git push on all repositories.
function ss-git-push {
	_ss-init || return
	for dir in "${_SS_GIT_REPOS[@]}"; do
		_runin "$dir" git push || return
	done
}

### Utilities ###

# Escapes backslashes, args -> stdout
function _escape-backslash {
	echo ${*//\\/\\\\}
}

# Wrapper around 'rm -I' that displays the path about to be deleted
function _rm-safely {
	echo "Removing $@ ..."
	rm -rf -I "$@"
}

# Tests if a Windows service exists
# arg1: service name
function _service-exists {
	sc qc "$1" > /dev/null
}

# Tests if a Windows service is running
# arg1: service name
function _service-running {
	sc query "$1" | grep --quiet "STATE.*RUNNING"
}

# Starts the services passed as arguments
# Services that do not exist are skipped.
# Services that are already running cause a failure.
function _start-services {
	local svc
	for svc in "$@"; do
		if _service-exists "$svc"; then
			net start "$svc" || return
		fi
	done
}

# Stops the services passed as arguments
# Services that are stopped or do not exist are skipped.
function _stop-services {
	local svc
	for svc in "$@"; do
		if _service-running "$svc"; then
			net stop "$svc" || return
		fi
	done
}

# Removes the services passed as arguments
# Services that do not exist are skipped. Services are stopped before being removed.
function _remove-services {
	local svc
	for svc in "$@"; do
		if _service-exists "$svc"; then
			_stop-services "$svc"
			sleep 1
			sc delete "$svc"
		fi
	done
}

# Runs a command in a given directory and preserves the $? of the command
# Example: "_runin /src/project mvn install"
function _runin {
	pushd $1
	shift
	"$@"
	local STATUS=$?
	popd
	return $STATUS
}

function _activate_java8 {
	if [[ -z $JAVA8_HOME ]]; then
		echo "JAVA8_HOME environment variable is not defined."
		return 1
	fi
	export JAVA_HOME="$JAVA8_HOME"
	export PATH="$(cygpath "$JAVA_HOME")/bin:$PATH"
}

function _activate_java7 {
	if [[ -z $JAVA7_HOME ]]; then
		echo "JAVA7_HOME environment variable is not defined."
		return 1
	fi
	export JAVA_HOME="$JAVA7_HOME"
	export PATH="$(cygpath "$JAVA_HOME")/bin:$PATH"
}
